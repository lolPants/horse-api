// Package Dependencies
const path = require('path')
const fs = require('fs-extra')
const express = require('express')
const app = express()

// Define Global Variables
let HORSE = ''
let LEGS = ''

app.get('/', (req, res) => {
  // Set readable text stream
  res.writeHead(200, { 'Content-Type': 'text/event-stream' })

  // Write the original horse
  res.write(HORSE)
  // Write legs forever
  setInterval(() => { res.write(LEGS) }, 10)
})

// Setup
const setup = async () => {
  // Get text files
  const BASE = path.join(__dirname, 'horse')
  HORSE = await fs.readFile(path.join(BASE, 'horse.txt'), 'utf8')
  LEGS = await fs.readFile(path.join(BASE, 'legs.txt'), 'utf8')

  // Append newlines
  HORSE += '\n'
  LEGS += '\n'

  // Start Express Server
  app.listen(process.env.PORT || 3000)
}
setup()
